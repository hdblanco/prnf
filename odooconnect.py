# -*- coding: utf-8 -*-
# codigo genérico del conector
import base64
import xmlrpclib
from datetime import datetime

import yaml

try:
    with open('config.yml', 'r') as f:
        config = yaml.load(f)
except:
    print """
    
    ERROR: No pudo leer archivo de configuracion. config.yml.
    Revise si se encuentra en el directorio  de instalacion de 
    prnfiscal.
    

    """
    raise SystemExit(0)

max_lineas = {}
doc_types = []
for docu in config['DOCUMENTS']:
    print docu, config['DOCUMENTS'][docu]
    try:
        max_lineas[docu] = config['DOCUMENTS'][docu]['maxlineas']
        doc_types.append(docu)
    except:
        print """
        ERROR: No está definida en el archivo config.ini la
        cantidad de lineas de  {}
        """.format(docu)

print 'Documentos a imprimir: {}'.format(', '.join(doc_types))


class i_data(object):
    try:
        username = config['DEFAULT']['username']
        pwd = base64.b64decode(config['DEFAULT']['password'])
        dbname = config['DEFAULT']['database']
        odoourl = config['DEFAULT']['url']
    except:
        print """

        ERROR:
        No se pudieron obtener parámetros de acceso. 
        Por favor revise que el archivo \"config.ini\" contenga la
        información de acceso.
            TIPS: la seccion \"[DEFAULT]\" debe existir.
                Dentro de ella deben estar los siguientes parametros:
                username, password, database y url.

        """
        raise SystemExit(0)
    
    sock_common = xmlrpclib.ServerProxy ('{}/xmlrpc/2/common'.format(odoourl))
    sock = xmlrpclib.ServerProxy ('{}/xmlrpc/2/object'.format(odoourl))
    uid = sock_common.authenticate(dbname, username, pwd, {})

    def get_invoice(self, max_lineas=max_lineas):

        args = [
            ['invoice_printed', 'not in', ['printed', 'old']],
            ['type', '=', 'out_invoice'],
            ['state', 'not in', ['draft', 'cancel']],
        ]

        puntoventa = config['DEFAULT']['puntoventa']
        # EN LUGAR DE IDENTIFICAR POR EL PUNTO DE VENTA, IDENTIFICO POR EL JOURNAL_ID
        if puntoventa:
        #    args.append(['sii_document_number', 'like', puntoventa+'-'])
            args.append(['journal_id', '=', int(puntoventa)])
        account_invoice_fields = [
            'id',
            'date_invoice',
            'date_due',
            'sii_document_class_id',
            'sii_document_number',
            'state', 
            'amount_tax',
            'amount_untaxed',
            'amount_total',
            'partner_id',
            'vat_discriminated',
            'responsability_id',
			'invoice_turn',
            # 'seller_id',
        ]

        res_partner_fields = [
            'name',
            'street',
            'vat',
            'city',
            'phone',
            'state_id',
        ]

        account_invoice_line_fields = [
            'origin',
            'uos_id',
            'create_date',
            'sequence',
            'price_unit',
            'price_subtotal',
            'write_uid',
            'partner_id',
            'printed_price_subtotal',
            'create_uid',
            'display_name',
            '__last_update',
            'vat_tax_ids',
            'company_id',
            'id',
            'account_analytic_id',
            'purchase_line_id',
            'other_taxes_amount',
            'vat_exempt_amount',
            'account_id',
            'invoice_line_tax_id',
            'discount',
            'write_date',
            'name',
            'vat_amount',
            'printed_price_unit',
            'printed_price_net',
            'quantity'
        ]

        taxes = [
            'ref_base_code_id',
            'domain',
            'ref_tax_code_id',
            'sequence',
            'base_sign',
            'include_base_amount',
            'id',
            'display_name',
            'applicable_type',
            'name',
            'tax_code_id',
            'parent_id',
            'ref_tax_sign',
            'type',
            'ref_base_sign',
            'description',
            'child_ids',
            'type_tax_use',
            'base_code_id',
            'account_analytic_paid_id',
            'active',
            'amount',
            'tax_sign',
            'price_include'
        ]



        account_invoice_line_tax_fields = [
            'invoice_line_id',
            'tax_id'
        ]

        account_tax_fields = ['id', 'amount']


        empty_line = {
            'product_id': ['', ''], 'sequence': 40, 'price_unit': '',
            'price_subtotal': '', 'discount': '', 'quantity': '', 'id': '',
            'name': ''}
        
        try:
            # print "consumiendo primer ws..."
        
            #print 'dbname: %s, uid: %s, pwd: %s' % (
            #   self.dbname, self.uid, self.pwd)
            invoice_id = self.sock.execute_kw(
                self.dbname, self.uid, self.pwd, 'account.invoice', 'search',
                [args], {})
            # print invoice_id
        except:
            print """

            ERROR:
            No se pudo efectuar la consulta. Revise si los parametros de 
            ingreso a su sistema en \"config.ini\" son los correctos.
            Serciorese de haber instalado el modulo \"invoice_printed\" 
            en su sistema Odoo.


            """
            raise SystemExit(0)

        invoices = self.sock.execute_kw(self.dbname, self.uid, self.pwd,
            'account.invoice', 'read', [invoice_id],
            {'fields':account_invoice_fields})
        invoice_data = {}
        for invoice in invoices:
            invoice_data["head"] = invoice

            partner = self.sock.execute_kw(self.dbname, self.uid, self.pwd,
                'res.partner', 'read', [invoice['partner_id'][0]],
                {'fields':res_partner_fields})
            
            invoice_data["partner"] = partner
            
            invoice_lines_args = [
                ['invoice_id', '=', invoice['id']],
            ]

            invoice_lines_ids = self.sock.execute_kw(
                self.dbname, self.uid, self.pwd, 'account.invoice.line',
                'search', [invoice_lines_args], {})
            i = 0
            invoice_data["lines"] = []
            for invoice_line_id in invoice_lines_ids:
                invoice_line = self.sock.execute_kw(
                    self.dbname, self.uid,
                    self.pwd, 'account.invoice.line', 'read',
                    [invoice_line_id], {'fields':account_invoice_line_fields})
                invoice_line['name'] = invoice_line['name'].encode("cp1252")
                # obtains tax percent using its code
                try:
                    invoice_tax = self.sock.execute_kw(
                    self.dbname, self.uid,
                    self.pwd, 'account.tax', 'read',
                    [invoice_line['invoice_line_tax_id'][0]], {'fields': taxes})
                except:
                    invoice_tax = {'amount': 0}
                invoice_line['tax_percent'] = invoice_tax['amount'] * 100
                invoice_data["lines"].append(invoice_line)
                i += 1
                # print invoice_line
                # print 'tax'
                # print invoice_line['invoice_line_tax_id'][0]
                # para ver los campos
                # invoice_taxes_fields = self.sock.execute_kw(
                #     self.dbname, self.uid,
                #     self.pwd, 'account.tax', 'fields_get',
                #     [], {'attributes': ['string', 'type']})
                # print 'tax'
                # raise SystemExit(0)
                print 'hay %s lineas' % i

            try:
                if config['PRINTER']['seguridad'] == "ON":
                    j = input ((
                        """PRNFISCAL: Por imprimir {} {}. Presione \"s\" para
                        continuar.""").format(
                            invoice_data["head"]["sii_document_class_id"][1],
                            invoice_data["head"]["sii_document_number"]))
                    if j[0] != "s":
                        print """Saliendo... Ingrese nuevamente para continuar
                        operando."""
                        raise SystemExit(0)
            except:
                pass

            print (
                "PRNFISCAL: {} Imprimiendo {} {}....").format(
                    datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
                    invoice_data["head"]["sii_document_class_id"][1],
                    invoice_data["head"]["sii_document_number"])
            print invoice_data["head"]["sii_document_class_id"]
            try:
                invoice_data['max_lineas'] = int(max_lineas[invoice_data["head"]["sii_document_class_id"][1]])
            except:
                if invoice_data["head"]["sii_document_class_id"][1] not in doc_types:
                    invoice_data['max_lineas'] = 0


        return invoice_data

    def update_invoice(self, id, model='account.invoice', **kwargs):
        
        ids = [id]
        print ids

        values = {
            'invoice_printed': u'printed',
            #'controller_cmnd': u'' if 'controller_cmnd' not in kwargs.keys() 
            #    else kwargs['controller_cmnd'],
            #'controller_ansr': u'' if 'controller_ansr' not in kwargs.keys() 
            #    else kwargs['controller_ansr'],
            #'controller_ansr': 'TEST mode'
        }
        print values


        return self.sock.execute_kw(
            self.dbname, self.uid, self.pwd, model, 'write', [ids, values])
