# -*- coding: utf-8 -*-
from cStringIO import StringIO
import subprocess
from elaphe import barcode
import PIL
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
# barcodefile = StringIO()
# tamaño de font = 30 (altura)
# tamaño de ancho del font = 65% de la altura
# = 30 * .56 = 16,8 aprox
# ancho = 660px
# margen: 10 * 2 = 20
# ancho del cod bar 640
# entran caracteres: 38
# GWIDTH = 38
BCWIDTH = 640
FSIZE = 30
CHARFACTOR = 0.56
GWIDTH = int(BCWIDTH / (FSIZE * CHARFACTOR))
BCMARGIN = 5

font = ImageFont.truetype("arial.ttf", FSIZE)

COMPANIA = 'Dantechnique Ltda.'
# 42 POSICIONES ES EL MÁXIMO EN UNA LINEA

product_toprint = ['123456789', '09999']
nombre_producto = 'Nombre del producto'

def wshift(text):
    """
    Función que calcula el centro horizontal
    y determina el pixel donde la leyenda debe comenzar
    """
    WIDTH = GWIDTH # maximo posible
    # FACTOR = 15 # factor de multiplicación pixel posición
    FACTOR = int(CHARFACTOR * FSIZE)
    OFFSET = int(CHARFACTOR * BCMARGIN) #28 # pixel donde comienza a escribir
    return ((WIDTH-len(text[:WIDTH]))/2)*FACTOR+OFFSET

def resizecode(image1):
    # basewidth = 300
    # img = Image.open('somepic.jpg')
    # wpercent = (basewidth/float(img.size[0]))
    # hsize = int((float(img.size[1])*float(wpercent)))
    # img = img.resize((basewidth,hsize), PIL.Image.ANTIALIAS)
    # img.save('sompic.jpg')
    pass

def cropbarcode(image1):
    pass

def pastelogo(function):
    """
    Función decoradora para agregar
    el Logo de la empresa
    """
    def inner(*args, **kwargs):
        image1 = function(args[0])
        img = Image.open('./danlogoverth.jpg', 'r')
        img_w, img_h = img.size
        background = Image.new('RGBA', (764, 236), (255, 255, 255, 255))
        bg_w, bg_h = background.size
        background.paste(img, (28, 5))
        background.paste(image1, (100, 0))
        return background
    return inner

def drawtextvar(texts):
    """
    Función decoradora que permite agregar
    un texto con una leyenda pasada por parámetros
    """
    def rdrawtv(function):
        def inner(*args, **kwargs):
            image1 = function(args[0])
            # print args
            # raise SystemExit(0)
            w, h = image1.size
            draw = ImageDraw.Draw(image1)
            i = 1
            for text in texts:
                draw.rectangle( (0 , (FSIZE + 3) * (i-1), w, (FSIZE + 3) * i), fill="white")
                draw.text((wshift(text) + 60, 2 + (FSIZE + 3) * (i-1)), text[:GWIDTH], (0, 0, 0), font=font)
                draw = ImageDraw.Draw(image1)
                i += 1
            return image1
        return inner
    return rdrawtv

def drawtextcode(function):
    """
    Función decoradora que permite agregar el 
    texto del código
    """
    def inner(*args, **kwargs):
        image1 = function(args[0])
        print args[0]
        w, h = image1.size
        print w, h
        draw = ImageDraw.Draw(image1)
        draw.rectangle( (0, h - (FSIZE + 3), w, h), fill="white")
        draw.text((wshift(product_toprint[0]) + 40, h - (FSIZE + 3)), product_toprint[0], (0, 0, 0), font=font)
        draw = ImageDraw.Draw(image1)
        return image1
    return inner

def drawcode(product_toprint):
    @pastelogo
    @drawtextvar([COMPANIA, product_toprint[1]])
    @drawtextcode
    def drawbarcode(product_toprint):
        image = barcode(
            'code128', product_toprint[0],
            options=dict(includetext=True), scale=4, margin=BCMARGIN)
        # image.save(barcodefile, 'PNG')
        w, h = image.size
        image1 = image.crop((0, 80, w, h-80))
        return image1
    return drawbarcode(product_toprint)

def printbarcode(image):
    image.save('img01.png')
    # image.show()
    # subprocess.check_call(["./brother_print"], shell=True)

def dbc(code_name):
    global product_toprint
    product_toprint = code_name
    printbarcode(drawcode(product_toprint))
